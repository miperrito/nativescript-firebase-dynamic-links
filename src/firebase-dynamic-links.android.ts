import * as appModule from "tns-core-modules/application";
import { firebase } from "./firebase-dynamic-links.common";

declare const com: any;
const gmsTasks = (<any>com.google.android.gms).tasks;

(() => {
    const getDynamicLinksCallback = new gmsTasks.OnSuccessListener({
        onSuccess: pendingDynamicLinkData => {
            if (pendingDynamicLinkData != null) {
                const deepLink = pendingDynamicLinkData.getLink().toString();
                const minimumAppVersion = pendingDynamicLinkData.getMinimumAppVersion();

                if (firebase._dynamicLinkCallback === null) {
                    firebase._cachedDynamicLink = { 
                        url: deepLink, 
                        minimumAppVersion: minimumAppVersion 
                    };
                } else {
                    setTimeout(function () {
                        firebase._dynamicLinkCallback({ url: deepLink, minimumAppVersion: minimumAppVersion });
                    }, 1);
                }
            }
        }
    });
    // note that this means we need to 'require()' the plugin before the app is loaded
    appModule.on(appModule.launchEvent, args => {
        com.google.firebase.dynamiclinks.FirebaseDynamicLinks.getInstance()
            .getDynamicLink(args.android).addOnSuccessListener(getDynamicLinksCallback);
    });
    appModule.android.on(appModule.AndroidApplication.activityNewIntentEvent, (args: any) => {
        com.google.firebase.dynamiclinks.FirebaseDynamicLinks.getInstance()
            .getDynamicLink(args.intent).addOnSuccessListener(getDynamicLinksCallback);
    })
    // appModule.on(appModule.resumeEvent, args => {
    //     com.google.firebase.dynamiclinks.FirebaseDynamicLinks.getInstance()
    //         .getDynamicLink(activity).addOnSuccessListener(getDynamicLinksCallback);
    // });
})();

firebase.addOnDynamicLinkReceivedCallback = callback => {
    return new Promise((resolve, reject) => {
        try {
            if (typeof (com.google.firebase.dynamiclinks) === "undefined") {
                reject("Uncomment dynamic links in the plugin's include.gradle first");
                return;
            }
  
            firebase._dynamicLinkCallback = callback;
  
            // if the app was launched from a dynamic link, process it now
            if (firebase._cachedDynamicLink) {
                callback(firebase._cachedDynamicLink);
                firebase._cachedDynamicLink = null;
            }
  
            resolve();
        } catch (ex) {
            console.log("Error in firebase.addOnDynamicLinkReceivedCallback: " + ex);
            reject(ex);
        }
    });
};

firebase.init = arg => {
    return new Promise((resolve, reject) => {
        if (firebase.initialized) {
            reject("Firebase already initialized");
            return;
        }
  
        firebase.initialized = true;
  
        const runInit = () => {
            arg = arg || {};
  
            // Firebase DynamicLink
            if (arg.onDynamicLinkCallback !== undefined) {
                firebase.addOnDynamicLinkReceivedCallback(arg.onDynamicLinkCallback);
            }

            resolve();
        };
  
        try {
            if (appModule.android.startActivity) {
                runInit();
            } else {
                // if this is called before application.start() wait for the event to fire
                appModule.on(appModule.launchEvent, runInit);
            }
        } catch (ex) {
            console.log("Error in firebase.init: " + ex);
            reject(ex);
        }
    });
};

module.exports = firebase;
