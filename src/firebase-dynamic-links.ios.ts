import { firebase } from './firebase-dynamic-links.common';
import * as application from 'tns-core-modules/application';

declare var FIRDynamicLinks: any;
declare var FIRApp: any;
declare var FIROptions: any;

firebase._configured = false;

NSNotificationCenter.defaultCenter.addObserverForNameObjectQueueUsingBlock(
    UIApplicationDidFinishLaunchingNotification, null, NSOperationQueue.mainQueue, appNotification => {
        if (!firebase._configured) {
            firebase._configured = true;
            if (typeof (FIRApp) !== "undefined") {
                FIRApp.configure();
            }
        }
    }
);

firebase.addOnDynamicLinkReceivedCallback = callback => {
    return new Promise((resolve, reject) => {
        try {
            firebase._dynamicLinkCallback = callback;

            // if the app was launched from a dynamic link, process it now
            if (firebase._cachedDynamicLink !== null && firebase._cachedDynamicLink && firebase._cachedDynamicLink.url) {
                callback(firebase._cachedDynamicLink);
                firebase._cachedDynamicLink = null;
            }

            resolve();
        } catch (ex) {
            console.log("Error in firebase.addOnDynamicLinkReceivedCallback: " + ex);
            reject(ex);
        }
    });
};

firebase.addAppDelegateMethods = appDelegate => {
    appDelegate.prototype.applicationContinueUserActivityRestorationHandler = (application, userActivity, restorationHandler) => {
        let result = false;
        result = FIRDynamicLinks.dynamicLinks().handleUniversalLinkCompletion(userActivity.webpageURL, (dynamicLink, error) => {
    
            console.log('dynamicLinks().handleUniversalLinkCompletion');
            if (dynamicLink !== null && dynamicLink.url !== null) {
              if (firebase._dynamicLinkCallback) {
                firebase._dynamicLinkCallback({
                  url: dynamicLink.url.absoluteString,
                  minimumAppVersion: dynamicLink.minimumAppVersion
                });
              } else {
                firebase._cachedDynamicLink = {
                  url: dynamicLink.url.absoluteString,
                  minimumAppVersion: dynamicLink.minimumAppVersion
                };
              }
            } else {
                console.log('dynamicLink is null || dynamicLink.url is null');
            }
        });
        return result;
    }
    
    appDelegate.prototype.applicationOpenURLSourceApplicationAnnotation = (application, url, sourceApplication, annotation) => {
        let result = false;
    
        const dynamicLink = FIRDynamicLinks.dynamicLinks().dynamicLinkFromCustomSchemeURL(url);
        console.log('dynamicLinks().applicationOpenURLSourceApplicationAnnotation');
        if (dynamicLink !== null && dynamicLink.url !== null) {
            firebase._cachedDynamicLink = {
                url: dynamicLink.url.absoluteString,
                minimumAppVersion: dynamicLink.minimumAppVersion
            };
            result = true;
        } else {
            console.log('dynamicLink is null || dynamicLink.url is null');
        }
    
        return result;
    };
    
    appDelegate.prototype.applicationOpenURLOptions = (application, url, options) => {
        let result = false;
    
        const dynamicLinks = FIRDynamicLinks.dynamicLinks();
        const dynamicLink = dynamicLinks.dynamicLinkFromCustomSchemeURL(url);
        console.log('dynamicLinks().applicationOpenURLOptions');
        if (dynamicLink && dynamicLink.url !== null) {
            if (firebase._dynamicLinkCallback) {
                firebase._dynamicLinkCallback({
                    url: dynamicLink.url.absoluteString,
                    minimumAppVersion: dynamicLink.minimumAppVersion
                });
            } else {
                firebase._cachedDynamicLink = {
                    url: dynamicLink.url.absoluteString,
                    minimumAppVersion: dynamicLink.minimumAppVersion
                };
            }
            result = true;
        } else {
            console.log('dynamicLink is null || dynamicLink.url is null');
        }
        return result;
    }
}

// This breaks in-app-messaging :(
function getAppDelegate() {
    // Play nice with other plugins by not completely ignoring anything already added to the appdelegate
    if (application.ios.delegate === undefined) {
    
        @ObjCClass(UIApplicationDelegate)
        class UIApplicationDelegateImpl extends UIResponder implements UIApplicationDelegate {
        }
    
        application.ios.delegate = UIApplicationDelegateImpl;
    }
    return application.ios.delegate;
}

firebase.addAppDelegateMethods(getAppDelegate());

firebase.init = arg => {
    return new Promise((resolve, reject) => {
        if (firebase.initialized) {
            reject("Firebase already initialized");
            return;
        }
  
        firebase.initialized = true;
  
        try {
            arg = arg || {};
  
            // if deeplinks are used, then for this scheme to work the use must have added the bundle as a scheme to their plist (this is in our docs)
            if (FIROptions && FIROptions.defaultOptions() !== null) {
                FIROptions.defaultOptions().deepLinkURLScheme = NSBundle.mainBundle.bundleIdentifier;
            }
    
            if (!firebase._configured) {
                firebase._configured = true;
                if (typeof (FIRApp) !== "undefined") {
                FIRApp.configure();
                }
            }
  
            // Firebase DynamicLink
            if (arg.onDynamicLinkCallback !== undefined) {
                firebase.addOnDynamicLinkReceivedCallback(arg.onDynamicLinkCallback);
            }
  
            resolve();
        } catch (ex) {
            console.log("Error in firebase.init: " + ex);
            reject(ex);
        }
    });
};

module.exports = firebase;