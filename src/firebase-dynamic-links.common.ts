export const firebase: any = {
  initialized: false,
  instance: null,
  _dynamicLinkCallback: null,
  dynamicLinks: {
    MATCH_CONFIDENCE: {
      WEAK: 0,
      STRONG: 1
    }
  }
};